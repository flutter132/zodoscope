import 'dart:ui';

class ZodiacSign {
  String name;
  String title;
  String cardPath;
  String logoPath;
  Color signColor;
  String signTimeSpan;

  ZodiacSign(
    this.name,
    this.title,
    this.cardPath,
    this.logoPath,
    this.signColor,
    this.signTimeSpan,
  );

  String get getName => name;
  String get getTitle => title;
  String get getCardPath => cardPath;
  String get getLogoPath => logoPath;
  Color get getColor => signColor;
  String get getSpan => signTimeSpan;
}

final ZodiacSign aquarius = ZodiacSign(
  "Aquarius",
  "水瓶座",
  "assets/images/aquarius.png",
  "assets/strokes/aquarius.png",
  Color(0xFF4297C6),
  "1/21 - 2/18",
);

final ZodiacSign pisces = ZodiacSign(
  "Pisces",
  "雙魚座",
  "assets/images/pisces.png",
  "assets/strokes/pisces.png",
  Color(0xFFDE4A46),
  "2/19 - 3/20",
);

final ZodiacSign aries = ZodiacSign(
  "Aries",
  "牡羊座",
  "assets/images/aries.png",
  "assets/strokes/aries.png",
  Color(0xFFEFA91B),
  "3/21 - 4/20",
);

final ZodiacSign taurus = ZodiacSign(
  "Taurus",
  "金牛座",
  "assets/images/taurus.png",
  "assets/strokes/taurus.png",
  Color(0xFFBF6F47),
  "4/21 - 5/21",
);

final ZodiacSign gemini = ZodiacSign(
  "Gemini",
  "雙子座",
  "assets/images/gemini.png",
  "assets/strokes/gemini.png",
  Color(0xFFE29A59),
  "5/22 - 6/21",
);

final ZodiacSign cancer = ZodiacSign(
  "Cancer",
  "巨蟹座",
  "assets/images/cancer.png",
  "assets/strokes/cancer.png",
  Color(0xFFD83554),
  "6/22 - 7/22",
);

final ZodiacSign leo = ZodiacSign(
  "Leo",
  "獅子座",
  "assets/images/leo.png",
  "assets/strokes/leo.png",
  Color(0xFFDB6412),
  "7/23 - 8/22",
);

final ZodiacSign virgo = ZodiacSign(
  "Virgo",
  "處女座",
  "assets/images/virgo.png",
  "assets/strokes/virgo.png",
  Color(0xFF179677),
  "8/23 - 9/22",
);

final ZodiacSign libra = ZodiacSign(
  "Libra",
  "天秤座",
  "assets/images/libra.png",
  "assets/strokes/libra.png",
  Color(0xFF26AEC4),
  "9/23 - 10/23",
);

final ZodiacSign scorpio = ZodiacSign(
  "Scorpio",
  "天蠍座",
  "assets/images/scorpio.png",
  "assets/strokes/scorpio.png",
  Color(0xFFC13A2F),
  "10/24 - 11/22",
);

final ZodiacSign sagittarius = ZodiacSign(
  "Sagittarius",
  "射手座",
  "assets/images/sagittarius.png",
  "assets/strokes/sagittarius.png",
  Color(0xFFB7294B),
  "11/23 - 12/21",
);

final ZodiacSign capricorn = ZodiacSign(
  "Capricorn",
  "摩羯座",
  "assets/images/capricorn.png",
  "assets/strokes/capricorn.png",
  Color(0xFF9276B7),
  "12/22 - 1/20",
);

final List<ZodiacSign> signs = [
  aquarius,
  capricorn,
  sagittarius,
  scorpio,
  libra,
  virgo,
  leo,
  cancer,
  gemini,
  taurus,
  aries,
  pisces,
];
